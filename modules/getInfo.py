from connect import methods


def login_id(id):
    query = """select login, org from AUTORIZ where """
    query += """sessionId = '""" + id + "'"
    res = methods.query(query)
    if not res:
        result = {
            'error': 'session id not found'
        }
        return result
    else:
        result = {
            'login': res[0][0],
            'org': res[0][1],
        }
        return result
